/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './app/App';
import {name as appName} from './app.json';

// Should be impored as early as possible so that variables are injected into
// stylesheet
import './app/styles/variables';

AppRegistry.registerComponent(appName, () => App);

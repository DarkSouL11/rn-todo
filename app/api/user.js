import localStorage from '../libs/localStorage';
import Validator from '../libs/validator';

const lsUserKey = 'api.currentUser';

const userValidator = new Validator({
  name: 'minLength:6',
  email: 'email',
  mobile: 'mobile',
  picture: 'required'
});

/**
 * Returns a promise which resolves when the user object is saved in
 * localstorage.
 *
 * @param {{}} user A user object in below defined schema
 *  `name` - User's name
 *  `email` - User's email
 *  `mobile` - User's mobile
 *  `picture` - User's profile picture
 */
function createUser(user) {
  const errors = userValidator.check(user);
  if (errors) throw errors;

  return localStorage.setItem(lsUserKey, user);
}

/**
 * Gets registered user from localStorage if available else returns `false`.
 */
function getUser() {
  return localStorage.getItem(lsUserKey);
}

function removeUser() {
  return localStorage.removeItem(lsUserKey);
}

/**
 * Returns a promise which resolves when the user object is updated in
 * localstorage.
 *
 * @param {{}} user
 */
async function updateUser(update) {
  const errors = userValidator.check(update);
  if (errors) throw errors;

  const user = await localStorage.getItem(lsUserKey);
  const updatedUser = { ...user, ...update };
  await localStorage.setItem(lsUserKey, updatedUser);
  return updatedUser;
}

export default {
  create: createUser,
  get: getUser,
  // Below api exposed only for testing purpose
  _remove: removeUser,
  update: updateUser,
}

import cloneDeep from 'lodash/cloneDeep';
import findIndex from 'lodash/findIndex';
import remove from 'lodash/remove';

import localStorage from '../libs/localStorage';
import Validator from '../libs/validator';

const lsTodosKey = "api.todos"

const defaultTodos = {
  // A helper which will be used to create unique id for every todo created
  totalAdded: 0,
  list: []
};

const todoValidator = new Validator({
  title: 'rangeLength:4,24'
});

function getFromLS() {
  return localStorage.getItem(lsTodosKey);
}

function setInLS(data) {
  return localStorage.setItem(lsTodosKey, data);
}

async function getNewTodoId() {
  const json = await getFromLS();
  json.totalAdded += 1;
  await setInLS(json);
  return json.totalAdded;
}

/**
 * @param {{}} data
 *  `title` - Todo title
 *  `description` (optional) - Detailed description of todo
 *  `completed` - A flag that indicates if todo is completed
 */
async function createTodo(data) {
  const errors = todoValidator.check(data);
  if (errors) throw errors;

  const id = await getNewTodoId();
  const todo = { id, ...data };
  const json = await getFromLS();
  json.list.push(todo);
  await setInLS(json);
  return todo;
}

async function getTodos() {
  let json = await getFromLS();
  if (!json) {
    // Make a copy and store
    json = cloneDeep(defaultTodos);
    await setInLS(json);
  }
  return json.list;
}

/**
 *
 * @param {number} id - Id of the todo to be removed
 */
async function removeTodo(id) {
  const json = await getFromLS();
  remove(json.list, o => o.id === id);
  await setInLS(json);
}

async function updateTodo(data) {
  const errors = todoValidator.check(data);
  if (errors) throw errors;

  const json = await getFromLS();
  const todoIndex = findIndex(json.list, o => o.id === data.id);
  const oldData = json.list[todoIndex];
  json.list[todoIndex] = { ...oldData, ...data };
  await setInLS(json);
}

export default {
  create: createTodo,
  get: getTodos,
  remove: removeTodo,
  update: updateTodo
}

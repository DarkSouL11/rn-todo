import React from 'react';
import { Provider } from 'mobx-react';

import AppBase from './AppBase';
import stores from './stores';

function App() {
  return (
    <Provider {...stores}>
      <AppBase />
    </Provider>
  );
}

export default App;

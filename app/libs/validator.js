class Validator {
  // Add new field types here
  static fieldTypes = {
    required: {
      isValid: (value, ...args) => !!value,
      message: 'Cannot be empty'
    },
    email : {
      pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,})+$/,
      message: 'Enter a valid email id.(Ex: example@test.com)'
    },
    mobile: {
      pattern: /^[6-9]\d{9}$/,
      message: 'Enter valid 10 digit phone number.'
    },
    minLength: {
      isValid: (value, ...args) => value.length >= args[0],
      message: 'Should be atleast {{0}} characters.'
    },
    maxLength: {
      isValid: (value, ...args) => value.length <= args[0],
      message: 'Should not be more than {{0}} characters.'
    },
    rangeLength: {
      isValid: (value, ...args) =>
        value.length >= args[0] && value.length <= args[1],
      message: 'Should be between {{0}} and {{1}} characters length.'
    },
    startsWith: {
      isValid: (value, ...args) =>
        typeof value === 'string' && value.startsWith(args[0]),
      message: 'Invalid input.'
    }
  }

  constructor(schema) {
    this._scheme = schema;
    this._init();
  }

  check(data) {
    const errors = {};

    Object.keys(data).forEach(key => {
      const value = data[key];
      const error = this._validate(key, value);

      if (error) {
        errors[key] = error;
      }
    });

    if (Object.keys(errors).length > 0) {
      return errors;
    } else {
      return null;
    }
  }

  _init() {
    this._validators = {};

    Object.keys(this._scheme).forEach(field => {
      let [type, args] = this._scheme[field].split(':');
      args = args ? args.split(',') : [];

      const { isValid, pattern, message } = Validator.fieldTypes[type];
      let resolvedMessage = message;
      args.forEach((arg, index) => {
        // Mimics `replaceAll`
        resolvedMessage = resolvedMessage.split(`{{${index}}}`).join(arg);
      });

      if (isValid) {
        this._validators[field] = value => {
          if (!isValid(value, ...args)) {
            return resolvedMessage;
          } else {
            return null;
          }
        }
      } else if (pattern) {
        this._validators[field] = value => {
          if (!pattern.test(value)) {
            return resolvedMessage;
          } else {
            return null;
          }
        }
      }
    });
  }

  _validate(key, value) {
    if (this._validators[key]) {
      return this._validators[key](value);
    }

    return null;
  }
}

export default Validator;

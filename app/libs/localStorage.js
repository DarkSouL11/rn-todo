import AsyncStorage from '@react-native-community/async-storage';

// Create your own instances if needed by exporting `LocalStorage`
export class LocalStorage {
  APP_STORAGE_PREFIX = '@todo-app';

  constructor(name) {
    this.name = name;
    // We form instance key by concating APP_STORE_PREFIX, ':' and name;
    this.instanceKey = `${this.APP_STORAGE_PREFIX}:${name}`;
  }

  async setItem(key, value) {
    let instanceStorage = JSON.parse(
      await AsyncStorage.getItem(this.instanceKey)
    );
    if (instanceStorage) {
      instanceStorage[key] = value;
    } else {
      instanceStorage = {};
      instanceStorage[key] = value;
    }
    await AsyncStorage.setItem(
      this.instanceKey,
      JSON.stringify(instanceStorage)
    );
  }

  async getItem(key) {
    let instanceStorage = JSON.parse(
      await AsyncStorage.getItem(this.instanceKey)
    );
    if (!instanceStorage) {
      return false;
    } else {
      if (instanceStorage[key]) {
        return instanceStorage[key];
      } else {
        return false;
      }
    }
  }

  async removeItem(key) {
    let instanceStorage = JSON.parse(
      await AsyncStorage.getItem(this.instanceKey)
    );
    delete instanceStorage[key];
    await AsyncStorage.setItem(
      this.instanceKey,
      JSON.stringify(instanceStorage)
    );
  }

  async clear() {
    await AsyncStorage.removeItem(this.instanceKey);
  }
}

// Default LocalStorage Instance
export default new LocalStorage('default');

import todosStore from './todosStore';
import userStore from './userStore';

export default {
  todosStore,
  userStore
}

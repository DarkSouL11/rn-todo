import { action, computed, decorate, flow, observable } from 'mobx';
import findIndex from 'lodash/findIndex';
import remove from 'lodash/remove';

import todosApi from '../api/todos';

class TodosStore {
  list = [];
  isLoading = false;
  loadError = null;
  isSubmitting = false;
  submitError = null;
  selectedTodoId = null;
  searchTerm = '';

  create = flow(function * (data) {
    this.isSubmitting = true;
    this.submitError = null;

    try {
      const json = yield todosApi.create({ ...data, completed: false });
      this.list.push(json);
      this.isSubmitting = false;
    } catch (error) {
      this.submitError = error;
      this.isSubmitting = false;

      throw error;
    }
  });

  loadTodos = flow(function * (){
    this.isLoading = true;
    this.loadError = null;

    try {
      const json = yield todosApi.get();
      this.list = json;
      this.isLoading = false;
    } catch (error) {
      this.loadError = error;
      this.isLoading = false;
    }
  });

  /**
   * Sets/unsets the completed status of a todo.
   *
   * @param {number} id
   * @param {boolean} completed
   */
  setCompleted = flow(function * (id, completed) {
    yield this.update(id, { completed });
  });

  /**
   * Updates a todo.
   *
   * @param {number} id
   * @param {boolean} completed
   */
  update = flow(function * (id, data) {
    this.isSubmitting = true;
    this.submitError = null;

    try {
      const todo = this.getTodo(id);
      yield todosApi.update({ ...todo, ...data });
      // Update in store only after successful update in localStorage
      const index = this.getTodoIndex(id);
      this.list[index] = { ...todo, ...data };
      this.list = this.list.slice();
      this.isSubmitting = false;
    } catch (error) {
      this.submitError = error;
      this.isSubmitting = false;

      throw error;
    }
  });

  remove = flow(function * (id) {
    this.isSubmitting = true;
    this.submitError = null;

    try {
      yield todosApi.remove(id);
      remove(this.list, o => o.id === id);
      this.list = this.list.slice();
      this.isSubmitting = false;
    } catch (error) {
      this.submitError = error;
      this.isSubmitting = false;

      throw error;
    }
  });

  clearErrors() {
    this.loadError = null;
    this.submitError = null;
  }

  setSelectedTodo(id) {
    this.selectedTodoId = id;
  }

  setSearchTerm(value) {
    this.searchTerm = value;
  }

  /**
   * Returns todo identified by the specified `id`.
   *
   * @param {number} id - Id of the todo that is being requested
   */
  getTodo(id) {
    return this.list.find(o => o.id === id);
  }

  getTodoIndex(id) {
    return findIndex(this.list, o => o.id === id);
  }

  get completedTodos() {
    return this.list.filter(o => o.completed);
  }

  get filteredTodos() {
    if (!this.searchTerm) return [];

    const lcSearchTerm = this.searchTerm.toLowerCase();
    return this.list.filter(o => o.title.toLowerCase().includes(lcSearchTerm));
  }

  get isEmpty() {
    return this.list.length === 0;
  }

  get pendingTodos() {
    return this.list.filter(o => !o.completed);
  }

  get selectedTodo() {
    if (!this.selectedTodoId) return null;

    return this.getTodo(this.selectedTodoId);
  }
}

decorate(TodosStore, {
  list: observable,
  isLoading: observable,
  loadError: observable,
  isSubmitting: observable,
  submitError: observable,
  searchTerm: observable,
  selectedTodoId: observable,
  clearErrors: action,
  setSelectedTodo: action,
  setSearchTerm: action,
  completedTodos: computed,
  filteredTodos: computed,
  isEmpty: computed,
  pendingTodos: computed,
  selectedTodo: computed
});

export default new TodosStore();

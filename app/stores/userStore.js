import { observable, flow, decorate } from 'mobx';

import userApi from '../api/user';

class UserStore {
  currentUser = null;
  isLoaded = false;
  isSubmitting = false;
  submitError = null;

  create = flow(function * (data) {
    this.isSubmitting = true;
    this.submitError = false;

    try {
      yield userApi.create(data);
      this.currentUser = data;
      this.isSubmitting = false;
    } catch (error) {
      this.submitError = error;
      this.isSubmitting = false;
    }
  });

  update = flow(function * (data) {
    this.isSubmitting = true;
    this.submitError = false;

    try {
      const updatedUser = yield userApi.update(data);
      this.currentUser = updatedUser;
      this.isSubmitting = false;
    } catch (error) {
      console.log(error);
      this.submitError = error;
      this.isSubmitting = false;

      throw error;
    }
  });

  // Below function is expected to run without errors as we are not loading
  // user from network so it is not wrapped in try...catch block
  loadUserFromLocalStorage = flow(function * () {
    // yield userApi._remove();
    this.currentUser = yield userApi.get();
    this.isLoaded = true;
  });
}

decorate(UserStore, {
  currentUser: observable.ref,
  isLoaded: observable,
  isSubmitting: observable,
  submitError: observable.ref
});

export default new UserStore();

import StyleSheet from 'react-native-extended-stylesheet';

export default StyleSheet.create({
  fill: {
    flexGrow: 1
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
});

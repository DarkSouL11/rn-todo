import StyleSheet from 'react-native-extended-stylesheet';

const variables = {
  // Colors
  $primaryColor: '#6d68df',
  $errorColor: '#cc0000',
  $textColor: '#515151',
  $lightColor: '#ffffff',
  $hintColor: '#828282',
  $borderColor: '#e2e2e2',
  $navItemColor: '#f2f2f2',

  // Font and Icon Sizes
  $fontXSmall: 12,
  $fontSmall: 14,
  $fontNormal: 16,
  $fontLarge: 24,
  $fontXLarge: 32,
  $iconXSmall: 14,
  $iconSmall: 16,
  $iconNormal: 20,
  $iconLarge: 24,
  $iconXLarge: 30,

  $buttonElevation: 1,
  $cardElevation: 2,
  $borderRadius: 4,
  $spacing: 10
};

StyleSheet.build(variables);

export default variables;

import React from 'react';
import { Image, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import Text from '../../components/general/Text';
import mobxify from '../../components/hoc/mobxify';
import Spacer from '../../components/general/Spacer';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {

  },
  profile: {
    padding: '$spacing * 2',
    backgroundColor: '$primaryColor'
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  text: {
    color: '$lightColor'
  },
  navItem: {
    backgroundColor: '$navItemColor',
    padding: '$spacing',
    marginTop: '$spacing / 2',
    marginBottom: '$spacing / 2',
  }
});

function DrawerScreen({ navigation, userStore: store }) {
  return (
    <View style={styles.container}>
      <View style={styles.profile}>
        <Image
          resizeMode="cover"
          style={styles.avatar}
          source={{ uri: store.currentUser.picture }}
        />
        <Spacer />
        <Text style={styles.text}>
          {store.currentUser.name}
        </Text>
        <Text size="small" style={styles.text}>
          {store.currentUser.email}
        </Text>
      </View>
      <TouchableNativeFeedback onPress={() => navigation.navigate('profile')}>
        <View style={styles.navItem}>
          <Text>Edit Profile</Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
}

export default mobxify('userStore')(DrawerScreen);

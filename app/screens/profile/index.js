import React from 'react';
import { ToastAndroid, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import ProfileForm from '../../components/ProfileForm';
import mobxify from '../../components/hoc/mobxify';

const styles = StyleSheet.create({
  container: {
    padding: '$spacing'
  }
});

class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Edit Profile'
  };

  _handleSubmit = async (user) => {
    const { userStore: store, navigation } = this.props;
    try {
      await store.update(user);
      ToastAndroid.show('Profile updated successfully', ToastAndroid.SHORT);
      navigation.navigate('todos');
    } catch (error) {
      // Ignore
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ProfileForm
          onSubmit={this._handleSubmit}
          submitLabel="Update"
        />
      </View>
    );
  }
}

export default mobxify('userStore')(ProfileScreen);

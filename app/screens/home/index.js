import React from 'react';
import { Dimensions } from 'react-native';
import ActionButton from 'react-native-action-button';
import BottomSheet from "react-native-raw-bottom-sheet";
import StyleSheet from 'react-native-extended-stylesheet';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';

import HintLayout from '../../components/general/HintLayout';
import IconButton from '../../components/general/IconButton';
import LoadingLayout from '../../components/general/LoadingLayout';
import mobxify from '../../components/hoc/mobxify';
import TodoList from '../../components/TodoList';
import NewTodo from './NewTodo';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: '$primaryColor'
  }
});

function P({ todosStore: store }) {
  return <TodoList list={store.pendingTodos} />;
}

function C({ todosStore: store }) {
  return <TodoList list={store.completedTodos} />;
}

const PendingTodos = mobxify('todosStore')(P);
const CompletedTodos = mobxify('todosStore')(C);

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Simple Todo',
    headerStyle: {
      backgroundColor: variables.$primaryColor,
      elevation: 0
    },
    headerLeft: (
      <IconButton
        elevation={0}
        icon="menu"
        onPress={() => navigation.toggleDrawer()}
      />
    ),
    headerRight: (
      <IconButton
        elevation={0}
        icon="search"
        onPress={() => navigation.push('search')}
      />
    ),
  });

  _bottomSheetRef = null;

  state = {
    index: 0,
    routes: [
      { key: 'pending', title: 'Pending' },
      { key: 'completed', title: 'Completed' },
    ],
  };

  componentDidMount() {
    this.props.todosStore.loadTodos();
  }

  _tabViewUi() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          pending: PendingTodos,
          completed: CompletedTodos
        })}
        renderTabBar={props =>
          <TabBar
            {...props}
            style={styles.tabBar}
          />
        }
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
      />
    );
  }

  _ui() {
    const { todosStore: store } = this.props;
    if (store.isLoading) {
      return <LoadingLayout />;
    } else if (store.loadError) {
      return (
        <HintLayout
          message="Failed to load todos"
          actionLabel="Try Again"
          actionFn={() => store.loadTodos()}
        />
      );
    } else if (store.isEmpty) {
      return (
        <HintLayout
          message="Click below to add todos"
          actionLabel="New Todo"
          actionFn={() => this._bottomSheetRef.open()}
        />
      );
    } else {
      return (
        <React.Fragment>
          {this._tabViewUi()}
          <ActionButton
            buttonColor={variables.$primaryColor}
            onPress={() => this._bottomSheetRef.open()}
          />
        </React.Fragment>
      );
    }
  }

  _newTodoUi() {
    return (
      <BottomSheet
        ref={ref => this._bottomSheetRef = ref}
        height={100}
        duration={250}
        customStyles={{
          container: {
            justifyContent: 'center'
          }
        }}
      >
        <NewTodo onSuccess={() => this._bottomSheetRef.close()} />
      </BottomSheet>
    );
  }

  render () {
    return (
      <React.Fragment>
        {this._ui()}
        {this._newTodoUi()}
      </React.Fragment>
    );
  }
}

export default mobxify('todosStore')(HomeScreen);

import React from 'react';
import { ToastAndroid, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import IconButton from '../../components/general/IconButton';
import Input from '../../components/form/Input';
import mobxify from '../../components/hoc/mobxify';
import Spacer from '../../components/general/Spacer';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: '$spacing'
  },
  input: {
    flexGrow: 1
  }
});

class NewTodo extends React.Component {
  state = {
    title: '',
    description: ''
  };

  _handleChange = (value, name) => this.setState({ [name]: value });

  _handleSubmit = async () => {
    const { todosStore: store, onSuccess } = this.props;
    try {
      const newTodo = {...this.state};
      await store.create(newTodo);
      ToastAndroid.show('Todo created successfully', ToastAndroid.SHORT);
      onSuccess && onSuccess();
    } catch (error) {
      // Ignore
    }
  }

  render() {
    const { todosStore: store } = this.props;
    const errors = store.submitError || {};

    return (
      <View style={styles.container}>
        <Input
          autoFocus
          error={errors.title}
          name="title"
          onChange={this._handleChange}
          placeholder="Todo title"
          keyboardType="default"
          style={styles.input}
        />
        <Spacer />
        <IconButton
          disabled={store.isSubmitting}
          icon="send"
          onPress={this._handleSubmit}
        />
      </View>
    );
  }
}

export default mobxify('todosStore')(NewTodo);

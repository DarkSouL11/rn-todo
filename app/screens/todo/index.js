import React from 'react';
import { Alert, KeyboardAvoidingView, ToastAndroid } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import Button from '../../components/general/Button';
import mobxify from '../../components/hoc/mobxify';
import IconButton from '../../components/general/IconButton';
import Input from '../../components/form/Input';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    padding: '$spacing',
  },
});

class TodoScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Edit Todo',
    headerRight: (
      <IconButton
        elevation={0}
        icon="delete"
        onPress={navigation.getParam('handleDelete')}
      />
    ),
  });

  constructor(props) {
    super(props);
    const { selectedTodo } = props.todosStore;
    this.state = {
      title: selectedTodo ? selectedTodo.title :  '',
      description: selectedTodo ? selectedTodo.description :  ''
    };
  }

  componentDidMount() {
    const { todosStore: store, navigation } = this.props;
    navigation.setParams({ handleDelete: this._handleDelete });
    store.clearErrors();
  }

  _handleDelete = () => {
    Alert.alert(
      'Are You Sure?',
      'Proceeding will delete this todo and cannot be restored again!',
      [
        {
          text: 'Yes, Proceed',
          onPress: async () => {
            const { todosStore: store, navigation } = this.props;
            await store.remove(store.selectedTodoId);
            ToastAndroid.show('Todo deleted successfully', ToastAndroid.SHORT);
            navigation.pop();
          }
        },
        { text: 'No, Go Back' },
      ],
      { cancelable: true },
    );
  }

  _handleChange = (value, name) => {
    this.setState({[name]: value});
  }

  _handleSubmit = async () => {
    try {
      const todo = {...this.state};
      const { todosStore: store, navigation } = this.props;
      await store.update(store.selectedTodoId, todo);
      ToastAndroid.show('Todo updated successfully', ToastAndroid.SHORT);
      store.setSelectedTodo(null);
      navigation.pop();
    } catch (error) {
      console.log('print', error);
      // Ignore
    }
  }

  render () {
    const { title, description } = this.state;
    const { todosStore: store } = this.props;
    const errors = store.submitError || {};

    return (
      <KeyboardAvoidingView
        behavior="padding"
        enabled
        style={styles.container}
      >
        <Input
          error={errors.title}
          name="title"
          onChange={this._handleChange}
          placeholder="Todo title"
          value={title}
        />
        <Input
          name="description"
          onChange={this._handleChange}
          placeholder="Todo description"
          value={description}
        />
        <Button
          disabled={store.isSubmitting}
          title="Update"
          onPress={this._handleSubmit}
        />
      </KeyboardAvoidingView>
    );
  }
}


export default mobxify('todosStore')(TodoScreen);

import React from 'react';
import { TextInput } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import mobxify from '../../components/hoc/mobxify';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  input: {
    color: '$lightColor'
  }
});

function SearchInput({ todosStore: store }) {
  return (
    <TextInput
      autoFocus
      onChangeText={value => store.setSearchTerm(value)}
      placeholder="Search by title"
      placeholderTextColor={variables.$lightColor}
      inlineImageLeft="search_icon"
      returnKeyType="search"
      selectionColor={variables.$lightColor}
      style={styles.input}
      value={store.searchTerm}
    />
  );
}

export default mobxify('todosStore')(SearchInput);

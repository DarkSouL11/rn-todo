import React from 'react';

import mobxify from '../../components/hoc/mobxify';
import HintLayout from '../../components/general/HintLayout';
import TodoList from '../../components/TodoList';
import SearchInput from './SearchInput';

class SearchScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <SearchInput />
  };

  render() {
    const { todosStore: store } = this.props;

    if (!store.searchTerm) {
      return (
        <HintLayout
          message="Start typing to search for todos by title"
        />
      );
    } else {
      return <TodoList list={store.filteredTodos} />;
    }
  }
}

export default mobxify('todosStore')(SearchScreen);

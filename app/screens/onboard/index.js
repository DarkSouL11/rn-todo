import React from 'react';
import { KeyboardAvoidingView, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import Button from '../../components/general/Button';
import mobxify from '../../components/hoc/mobxify';
import ImageInput from '../../components/form/ImageInput';
import Input from '../../components/form/Input';
import Text from '../../components/general/Text';
import variables from '../../styles/variables';
import ProfileForm from '../../components/ProfileForm';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '$primaryColor',
    padding: '$spacing',
    justifyContent: 'center',
  },
  header: {
    padding: '$spacing',
    marginBottom: '$spacing * 2'
  },
  title: {
    textAlign: 'center',
  }
});

class OnboardScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  _handleSubmit = (user) => {
    const { userStore: store } = this.props;
    store.create(user);
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text
            color={variables.$lightColor}
            size="xLarge"
            style={styles.title}
          >
            Simple Todo
          </Text>
        </View>
        <ProfileForm
          isLight
          onSubmit={this._handleSubmit}
          submitLabel="Get Started"
        />
      </View>
    );
  }
}


export default mobxify('userStore')(OnboardScreen);

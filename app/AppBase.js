import React from 'react';

import LoadingLayout from './components/general/LoadingLayout';
import mobxify from './components/hoc/mobxify';
import { LoggedInContainer, OnboardingContainer } from './containers';

class AppBase extends React.Component {
  componentDidMount () {
    this.props.userStore.loadUserFromLocalStorage();
  }

  render() {
    const { userStore: store } = this.props;

    if (store.isLoaded) {
      if (store.currentUser) {
        return <LoggedInContainer />;
      } else {
        return <OnboardingContainer />;
      }
    } else {
      return <LoadingLayout />;
    }
  }
}

export default mobxify('userStore')(AppBase);

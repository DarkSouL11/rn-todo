import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator
} from 'react-navigation';

import variables from './styles/variables';
import DrawerScreen from './screens/drawer';
import HomeScreen from './screens/home';
import OnboardScreen from './screens/onboard';
import ProfileScreen from './screens/profile';
import SearchScreen from './screens/search';
import TodoScreen from './screens/todo';

export const defaultNavigationOptions = {
  headerStyle: {
    backgroundColor: variables.$primaryColor
  },
  headerTintColor: variables.$lightColor
};

const OnboardingNavigator = createStackNavigator(
  { onboard: OnboardScreen },
  { initialRouteName: 'onboard', defaultNavigationOptions }
);

const StackNavigator = createStackNavigator(
  {
    todos: HomeScreen,
    todo: TodoScreen,
    profile: ProfileScreen,
    search: SearchScreen
  },
  { initialRouteName: 'todos', defaultNavigationOptions }
);

const LoggedInNavigator = createDrawerNavigator(
  { home: StackNavigator },
  {
    initialRouteName: 'home',
    contentComponent: DrawerScreen
  }
);

export const OnboardingContainer = createAppContainer(OnboardingNavigator);

export const LoggedInContainer = createAppContainer(LoggedInNavigator);

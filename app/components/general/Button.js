import React from 'react';
import { TouchableNativeFeedback, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import Text from './Text';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '$primaryColor',
    borderRadius: '$borderRadius',
    padding: '$spacing',
    elevation: '$buttonElevation'
  },
  title: {
    textTransform: 'uppercase',
    textAlign: 'center'
  }
});

function Button({ title, color, textColor, style, ...props }) {
  return (
    <TouchableNativeFeedback {...props}>
      <View
        style={[
          styles.container,
          style,
          color && {backgroundColor: color}
        ]}
      >
        <Text
          color={textColor || variables.$lightColor}
          style={styles.title}
        >
          {title}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

export default Button;

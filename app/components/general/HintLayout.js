import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import StyleSheet from 'react-native-extended-stylesheet';

import Button from './Button';
import Text from './Text';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    padding: '$spacing',
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    textAlign: 'center',
    marginBottom: '$spacing'
  }
});

function HintLayout({ message, actionLabel, actionFn }) {
  return (
    <View style={styles.container}>
      <Text style={styles.message}>{message}</Text>
      {actionLabel && actionFn && (
        <Button
          title={actionLabel}
          onPress={actionFn}
        />
      )}
    </View>
  )
}

HintLayout.propTypes = {
  message: PropTypes.string.isRequired,
  actionFn: PropTypes.func,
  actionLabel: PropTypes.string
}

export default HintLayout;

import React from 'react';
import { View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

const styles = StyleSheet.create({
  container: {
    width: '$spacing',
    height: '$spacing'
  }
})

function Spacer() {
  return <View style={styles.container} />;
}

export default Spacer;

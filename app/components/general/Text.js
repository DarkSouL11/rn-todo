import React, { PureComponent } from 'react';
import { Text as NativeText } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

const styles = StyleSheet.create({
  text: {
    color: '$textColor',
    fontSize: '$fontNormal'
  }
});

const sizeEnums = {
  'small': StyleSheet.value('$fontSmall'),
  'normal': StyleSheet.value('$fontNormal'),
  'large': StyleSheet.value('$fontLarge'),
  'xLarge': StyleSheet.value('$fontXLarge'),
  'xSmall': StyleSheet.value('$fontXSmall')
};

/**
 * Text component to be used in the app with minor modifications to
 * react native's default Text component which accepts extra params
 * like color and if nothing specified will use default text color of the app
 *
 * Usage:
 *    <Text color={optionallySpecifyTextColor} {...allOtherTextProps} />
 *
 */
class Text extends PureComponent {
  render() {
    const {
      style = {},
      color,
      size,
      ...remainingProps
    } = this.props;

    let textStyle = [styles.text];

    if (color) textStyle.push({color});

    if (size && typeof size === 'string' && sizeEnums[size]) {
      textStyle.push({fontSize: sizeEnums[size]});
    } else if (size && typeof size === 'number') {
      textStyle.push({fontSize: size});
    }

    textStyle.push(style);

    return (
      <NativeText style={textStyle} {...remainingProps} />
    );
  }
}

export default Text;

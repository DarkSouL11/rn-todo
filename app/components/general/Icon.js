import React from 'react';
import StyleSheet from 'react-native-extended-stylesheet';

import Icon from 'react-native-vector-icons/MaterialIcons';

const iconSizeMap = {
  'xSmall': StyleSheet.value('$iconXSmall'),
  'small': StyleSheet.value('$iconSmall'),
  'normal': StyleSheet.value('$iconNormal'),
  'large': StyleSheet.value('$iconLarge'),
  'xLarge': StyleSheet.value('$iconXLarge')
};

const defaultIconColor = StyleSheet.value('$textColor');

/**
 * React native vector icon component modified to have default color as app
 * default text color
 * @param {String} color Color of icon
 */
function AppIcon({
  size = iconSizeMap.normal,
  color = defaultIconColor,
  ...props
}) {

  function getIconSize(sz){
    if (typeof sz === 'string') return iconSizeMap[sz] || iconSizeMap.normal;
    return sz;
  }

  return (
    <Icon
      size={getIconSize(size)}
      color={color}
      {...props}
    />
  );
}

export default AppIcon;

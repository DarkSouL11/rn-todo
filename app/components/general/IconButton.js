import React from 'react';
import { TouchableNativeFeedback, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import Icon from './Icon';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  root: {
    borderRadius: 50,
  },
  container: {
    backgroundColor: '$primaryColor',
    borderRadius: 50,
    elevation: '$buttonElevation',
    padding: '$spacing',
  },
  title: {
    textAlign: 'center'
  }
});

function IconButton({ icon, color, elevation, textColor, style, ...props }) {
  return (
    <View style={styles.root}>
      <TouchableNativeFeedback
        useForeground
        background={TouchableNativeFeedback.Ripple(
          '#ffffff', true
        )}
        {...props}
      >
        <View
          style={[
            styles.container,
            style,
            color && { backgroundColor: color },
            (elevation !== null) && { elevation }
          ]}
        >
          <Icon color={textColor || variables.$lightColor} name={icon} />
        </View>
      </TouchableNativeFeedback>
    </View>
  );
}

export default IconButton;

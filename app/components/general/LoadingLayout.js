import React from 'react';
import { ActivityIndicator, View } from 'react-native';

import commonStyles from '../../styles/common';
import variables from '../../styles/variables';

function LoadingLayout() {
  return (
    <View style={[commonStyles.fill, commonStyles.center]}>
      <ActivityIndicator color={variables.$primaryColor} size="large" />
    </View>
  )
}

export default LoadingLayout;

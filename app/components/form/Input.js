import React from 'react';
import { TextInput, View } from 'react-native';
import PropTypes from 'prop-types';
import StyleSheet from 'react-native-extended-stylesheet';
import omit from 'lodash/omit';

import Text from '../general/Text';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    paddingBottom: '$spacing * 1.5',
    marginBottom: '$spacing',
  },
  light: {
    color: '$lightColor',
    borderColor: '$lightColor'
  },
  input: {
    color: '$textColor'
  },
  error: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    color: '$errorColor'
  }
});

class Input extends React.Component {
  static omitProps =
    ['onChange', 'error', 'isLight', 'name', 'style', 'value'];

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ''
    }
  }

  _handleChange = value => {
    const { name, onChange } = this.props;
    this.setState({ value });
    onChange && onChange(value, name);
  }

  render() {
    const { value } = this.state;
    const { error, isLight, style } = this.props;
    const color = isLight ? variables.$lightColor : variables.$textColor;
    const passThroughProps = omit(this.props, Input.omitProps);

    return (
      <View style={[styles.container, isLight && styles.light, style]}>
        <TextInput
          {...passThroughProps}
          onChangeText={this._handleChange}
          paddingBottom={variables.$spacing}
          placeholderTextColor={color}
          selectionColor={color}
          style={[styles.input, isLight && styles.light]}
          underlineColorAndroid={color}
          value={value}
        />
        <Text numberOfLines={1} size="small" style={styles.error}>
          {error}
        </Text>
      </View>
    );
  }
}

Input.propTypes = {
  onChange: PropTypes.func,
  error: PropTypes.string,
  isLight: PropTypes.bool,
  name: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.string,
}

export default Input;

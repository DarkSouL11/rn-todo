import React from 'react';
import { Image, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import ImagePicker from 'react-native-image-picker';

import IconButton from '../general/IconButton';
import placholderImage from '../../images/placeholder.png';
import Text from '../general/Text';
import variables from '../../styles/variables';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '$spacing',
    paddingBottom: '$spacing * 2',
    marginBottom: '$spacing',
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  imageWrapper: {
    position: 'relative',
  },
  button: {
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  error: {
    position: 'absolute',
    color: '$errorColor',
    left: 0,
    right: 0,
    bottom: 0,
    textAlign: 'center',
  }
});

class ImageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ''
    };
  }

  _handleChange = value => {
    this.setState({ value });
    this.onChange && this.onChange(value);
  }

  _pickImage = () => {
    const { name, onChange } = this.props;
    ImagePicker.showImagePicker({ title: 'Select a Image'}, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const base64 = `data:${response.type};base64,${response.data}`;
        this.setState({ value: base64 });
        onChange && onChange(base64, name);
      }
    });
  }

  render() {
    const { value } = this.state;
    const { error, isLight } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image
            resizeMode="cover"
            source={value ? { uri: value } : placholderImage}
            style={styles.image}
          />
          <IconButton
            icon={value ? 'edit' : 'add'}
            color={isLight && variables.$lightColor}
            textColor={isLight && variables.$primaryColor}
            style={styles.button}
            onPress={this._pickImage}
          />
        </View>
        <Text size="small" style={styles.error}>
          {error}
        </Text>
      </View>
    );
  }
}

export default ImageInput;

import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import PropTypes from 'prop-types';
import StyleSheet from 'react-native-extended-stylesheet';

import Button from './general/Button';
import ImageInput from './form/ImageInput';
import Input from './form/Input';
import mobxify from './hoc/mobxify';
import variables from '../styles/variables';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
  }
});

class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    const { userStore: store } = props;
    const user = store.currentUser || {};
    const { name, email, mobile, picture } = user;
    this.state = {
      name: name || '',
      email: email || '',
      mobile: mobile || '',
      picture: picture || ''
    };
  }

  _handleChange = (value, name) => {
    this.setState({[name]: value});
  }

  _handleSubmit = () => {
    const user = {...this.state};
    this.props.onSubmit(user);
  }

  render() {
    const { name, email, mobile, picture } = this.state;
    const { userStore: store, isLight, submitLabel } = this.props;
    const errors = store.submitError || {};

    return (
      <KeyboardAvoidingView
        behavior="padding"
        enabled
        style={styles.container}
      >
        <ImageInput
          error={errors.picture}
          isLight={isLight}
          name="picture"
          onChange={this._handleChange}
          value={picture}
        />
        <Input
          error={errors.name}
          isLight={isLight}
          name="name"
          onChange={this._handleChange}
          placeholder="Name"
          keyboardType="default"
          value={name}
        />
        <Input
          error={errors.email}
          isLight={isLight}
          name="email"
          onChange={this._handleChange}
          placeholder="Email"
          keyboardType="email-address"
          value={email}
        />
        <Input
          error={errors.mobile}
          isLight={isLight}
          name="mobile"
          onChange={this._handleChange}
          placeholder="Mobile"
          keyboardType="phone-pad"
          value={mobile}
        />
        <Button
          color={isLight && variables.$lightColor}
          disabled={store.isSubmitting}
          textColor={isLight && variables.$primaryColor}
          title={submitLabel}
          onPress={this._handleSubmit}
        />
      </KeyboardAvoidingView>
    );
  }
}

ProfileForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  submitLabel: PropTypes.string.isRequired,
  isLight: PropTypes.bool
};

export default mobxify('userStore')(ProfileForm);


import { inject, observer } from 'mobx-react';
import compose from 'lodash/fp/compose';

/**
 * A HOC that injects mobx stores to Component.
 *
 * @param {...string} stores
 * @returns {function(component: Object)}
 */
export function mobxify(...stores) {
  return compose(
    inject(...stores),
    observer
  );
}

export default mobxify;

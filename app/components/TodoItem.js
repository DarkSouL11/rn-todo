import React from 'react';
import { CheckBox, ToastAndroid, View } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { withNavigation } from 'react-navigation';
import compose from 'lodash/fp/compose';

import commonStyles from '../styles/common';
import IconButton from './general/IconButton';
import mobxify from './hoc/mobxify';
import Spacer from './general/Spacer';
import Text from './general/Text';
import variables from '../styles/variables';

const styles = StyleSheet.create({
  container: {
    padding: '$spacing',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '$spacing / 2',
    backgroundColor: '#ffffff',
    borderRadius: '$borderRadius',
    elevation: '$cardElevation'
  },
  completed: {
    color: '$hintColor',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid'
  }
});

function TodoItem({ item, todosStore: store, navigation }) {
  // We are sure that this can't cause error. So no error handling
  async function _setCompleted(value) {
    await store.setCompleted(item.id, value);
    ToastAndroid.show(
      `Todo marked as ${value ? 'completed' : 'incomplete'}`,
      ToastAndroid.SHORT
    );
  }

  function _openEditor() {
    store.setSelectedTodo(item.id);
    navigation.push('todo');
  }

  return (
    <View style={styles.container}>
      <CheckBox
        onValueChange={_setCompleted}
        value={item.completed}
      />
      <Spacer />
      <Text style={[commonStyles.fill, item.completed && styles.completed]}>
        {item.title}
      </Text>
      <Spacer />
      <IconButton
        color={variables.$lightColor}
        elevation={0}
        textColor={variables.$primaryColor}
        icon="edit"
        onPress={_openEditor}
      />
    </View>
  );
}

const hoc = compose(
  withNavigation,
  mobxify('todosStore')
);

export default hoc(TodoItem);

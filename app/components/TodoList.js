import React from 'react';
import { FlatList } from 'react-native';
import { observer } from 'mobx-react';
import StyleSheet from 'react-native-extended-stylesheet';

import HintLayout from './general/HintLayout';
import TodoItem from './TodoItem';


const styles = StyleSheet.create({
  container: {
    padding: '$spacing / 2',
  }
});

function TodoList({ list }) {
  if (list.length > 0) {
    return (
      <FlatList
        style={styles.container}
        data={list}
        keyExtractor={(item, index) => item.id.toString()}
        renderItem={item => <TodoItem item={item.item} />}
      />
    );
  } else {
    return (
      <HintLayout message="No todos available" />
    );
  }
}

export default observer(TodoList);
